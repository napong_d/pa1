package stopwatch;

/**
 * Double summation test for speed test.
 * @author Napong Dungduangsasitorn
 *
 */
public class SumDouble implements Runnable{


	/** double array of summation. */
	private Double[] sum ;
	/** number of times of the summation. */
	private int counter ;

	/**
	 * constructor.
	 * @param counter number of times of the summation
	 * @param arraySize size array that will be used in the test
	 */
	public SumDouble (int counter, int arraySize){
		this.counter = counter;
		this.sum = new Double[arraySize];
		for(int i = 0; i < arraySize; i++){
			sum[i] = new Double(i+1);
		}
	}

	/**
	 * run test.
	 */
	@Override
	public void run() {
		Double temp = new Double(0);
		for(int i = 0, count = 0; count < sum.length ; i++,count++){
			if(i>=sum.length) i = 0;
			temp = temp + (sum[i]);
		}
	}
	
	/**
	 * get test information.
	 * @return test information
	 */
	public String toString(){
		return String.format("sum Double objects with count = %d and array size = %d",counter,sum.length);
	}

}
