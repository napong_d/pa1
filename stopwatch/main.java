package stopwatch;

import stopwatch.TaskTimer;
/**
 * testing TaskTimer and Tasks.
 * @author Napong Dungduangsasitorn
 *
 */
public class main {

	public static void main (String[] args){
		/**
		 *  tasks to do.
		 */
		CharToString task1 = new CharToString(100000);
		CharToStringBuilder task2 = new CharToStringBuilder(100000);
		SumOfDouble task3 = new SumOfDouble(100000000,500000);
		SumDouble task4 = new SumDouble(100000000,500000);
		SumBigDecimal task5 = new SumBigDecimal(100000000,500000);
		
		/**
		 * get TaskTimer.
		 */
		TaskTimer timer = TaskTimer.getTaskTimer();
		
		/**
		 * run tasks and print results.
		 */
		timer.measureAndPrint(task1);
		timer.measureAndPrint(task2);
		timer.measureAndPrint(task3);
		timer.measureAndPrint(task4);
		timer.measureAndPrint(task5);
	}
}
