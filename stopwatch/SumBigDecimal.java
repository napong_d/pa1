package stopwatch;

import java.math.BigDecimal;


/**
 * BigDecimal summation test for speed test.
 * @author Napong dungduangsasitorn
 * 
 */
public class SumBigDecimal implements Runnable{

	/** array of BigDecimal of summation. */
	private BigDecimal[] sum;
	/** number of times of the summation. */
	private int counter;
	
	/**
	 * constructor.
	 * @param counter number of times of the summation.
	 * @param arraySize size array that will be used in the test
	 */
	public SumBigDecimal(int counter,int arraySize){
		this.counter = counter;
		this.sum = new BigDecimal[arraySize];
		for(int i = 0; i < arraySize; i++){
			sum[i] = new BigDecimal(i+1);
		}
	}
	
	/**
	 * run test.
	 */
	@Override
	public void run() {
		BigDecimal temp = new BigDecimal(0);
		for(int i = 0, count = 0 ; count < counter ; i++,count++){
			if(i >= sum.length) i = 0;
			temp = temp.add(sum[i]);
		}
	}

	/**
	 * get test information.
	 * @return test information
	 */
	public String toString(){
		return String.format(" sum BigDecimal with count = %d and array size = %d",counter,sum.length);
	}
}
