package stopwatch;

public class CharToStringBuilder implements Runnable{

	private static final char Char = 'a';
	/** number of times that string is append.*/
	private int counter;
	
	/**
	 * Constructor for this test.
	 * @param counter number of times that StringBuilder append.
	 */
	public CharToStringBuilder(int counter){
		this.counter = counter;
	}
	
	/**
	 * run test.
	 */
	@Override
	public void run() {
		StringBuilder stringbuilder = new StringBuilder();
		for(int i = 0; i < counter ; i++){
			stringbuilder = stringbuilder.append(Char);
		}
	}
	
	/**
	 * give information of this test.
	 * @return information of this test
	 */
	public String toString(){
		return String.format("append characters to a StringBuilder with count = %d", counter);
	}
}
