package stopwatch;

/**
 * measure the time that the task takes.
 * @author Napong Dungduangsasitorn
 *
 */

public class TaskTimer {

	/** measure the time. */
	private static final StopWatch timer = new StopWatch();
	
	/**
	 * constructor.
	 */
	private TaskTimer(){
		
	}
	
	/**
	 * run the task, measure the time and print the result.
	 * @param runnable.
	 */
	public static void measureAndPrint(Runnable runnable){
		timer.start();
		runnable.run();
		timer.stop();
		
		System.out.printf("Task : %s \n", runnable.toString());
		System.out.printf("Elapsed time : %.6f sec \n",timer.getElapsed());
	}
	
	/**
	 * get TaskTimer.
	 * @return TaskTimer
	 */
	public static TaskTimer getTaskTimer(){
		return new TaskTimer();
	}
}
