package stopwatch;

/**
 * measures time when starting program until stopping program.
 * @author Napong Dungduangsasitorn
 *
 */
public class StopWatch {
	
	/** convert nanoseconds to seconds. */
	private static final double nanoseconds = 1.0E-9;
	/** time that stopwatch start.*/
	private long startTime;
	/** time that stopwatch stop.*/
	private long stopTime;
	/** stopwatch status. */
	private boolean running;

	/**
	 * Constructor.
	 */
	public StopWatch(){
		this.running = false;
	}

	/**
	 *start the stopwatch.
	 */
	public void start(){
		if(!running){
			this.startTime = System.nanoTime();
			this.running = true;
		}
	}

	/**
	 * stop the stopwatch.
	 */
	public void stop(){
		if(running){
			this.stopTime = System.nanoTime();
			this.running = false;
		}
	}

	/**
	 * get the elasped time.
	 * @return time difference between start time and present time,
	 * or between start time and stop time if 
	 * stopwatch has stopped
	 */
	public double getElapsed(){
		if(running){
			long present = System.nanoTime();
			return (present-getStartTime())*nanoseconds;
		}
		return (getStopTime()-getStartTime())*nanoseconds;
	}

	/**
	 * get time seconds.
	 * @return nanoseconds in unit seconds.
	 */

	public double getNanoseconds(){
		return this.nanoseconds;
	}

	/**
	 * get start time.
	 * @return start time.
	 */
	public long getStartTime(){
		return this.startTime;
	}

	/**
	 * get stop time.
	 * @return stop time.
	 */
	public long getStopTime(){
		return this.stopTime;
	}

	/**
	 * checking that stopwatch is running.
	 * @return true if program is running.
	 */
	public boolean isRunning(){
		return this.running;
	}

}
